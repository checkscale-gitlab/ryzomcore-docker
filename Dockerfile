FROM ubuntu:18.04

RUN apt-get update && \
    apt-get install -y software-properties-common && \
    add-apt-repository ppa:ubuntu-toolchain-r/test && \
    apt-get update && \
    apt-get install -y \
        autoconf \
        automake \
        bison \
        build-essential \
        cmake \
        freeglut3-dev \
        g++-8 \
        gcc-8 \
        git \
        libavcodec-dev \
        libavdevice-dev \
        libavformat-dev \
        libcpptest-dev \
        libcurl4-openssl-dev \
        libfreetype6-dev \
        libgif-dev \
        libgsf-1-dev \
        libjpeg-dev \
        libluabind-dev \
        liblzma-dev \
        libmysqlclient-dev \
        libogg-dev \
        libopenal-dev \
        libpng-dev \
        libpostproc-dev \
        libsquish-dev \
        libssl-dev \
        libswscale-dev \
        libvorbis-dev \
        libxml2-dev \
        qtbase5-dev \
        qttools5-dev \
        qttools5-dev-tools && \
    update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-8 60 && \
    update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-8 60 && \
    git clone https://github.com/ryzom/ryzomcore.git /opt/ryzomcore

WORKDIR /opt/ryzomcore/build

RUN cmake -DWITH_STATIC=ON \
          -DWITH_NEL_TESTS=OFF \
          -DWITH_NEL_SAMPLES=ON \
          -DWITH_LUA51=ON -DWITH_RYZOM=ON \
          -DWITH_RYZOM_SERVER=ON \
          -DWITH_RYZOM_CLIENT=ON \
          -DWITH_RYZOM_TOOLS=ON \
          -DWITH_NEL_TOOLS=ON \
          -DWITH_NELNS=ON \
          -DWITH_NELNS_LOGIN_SYSTEM=ON \
          -DWITH_NELNS_SERVER=ON \
          -DWITH_QT5=ON \
          -DWITH_LIBGSF=ON .. && \
    make -j $(nproc)

